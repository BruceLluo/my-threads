"use server";

import { FilterQuery, SortOrder } from "mongoose";
import { revalidatePath } from "next/cache";

import User from "../models/user.model";

import { connectToDB } from "../mongoose";
import Thread from "../models/thread.model";
import Community from "../models/community.model";

// 用户信息
interface UserParams {
  userId: string;
  username: string;
  name: string;
  bio: string;
  image: string;
  path: string;
}

/** 更新用户信息 */
export async function updateUser({
  userId,
  bio,
  name,
  path,
  username,
  image,
}: UserParams): Promise<void> {
  try {
    connectToDB();

    await User.findOneAndUpdate(
      { id: userId },
      {
        username: username.toLowerCase(),
        name,
        bio,
        image,
        onboarded: true,
      },
      { upsert: true } // 允许插入或修改数据库数据
    );

    if (path === "/profile/edit") {
      revalidatePath(path);
    }
  } catch (error: any) {
    throw new Error(`Failed to create/update user: ${error.message}`);
  }
}

/** 查找当前用户数据 */
export async function fetchUser(userId: string) {
  try {
    connectToDB();

    return await User.findOne({ id: userId }).populate({
      path: "communities",
      model: Community,
    });
  } catch (error: any) {
    throw new Error(`Failed to fetch user: ${error.message}`);
  }
}

/** 获取用户帖子 */
export async function fetchUserPosts(userId: string) {
  try {
    connectToDB();

    // 找出指定userId用户的所有帖子
    const threads = await User.findOne({ id: userId }).populate({
      path: "threads",
      model: Thread,
      populate: [
        {
          path: "community",
          model: Community,
          select: "name id image _id", // Select the "name" and "_id" fields from the "Community" model
        },
        {
          path: "children",
          model: Thread,
          populate: {
            path: "author",
            model: User,
            select: "name image id", // Select the "name" and "_id" fields from the "User" model
          },
        },
      ],
    });
    return threads;
  } catch (error) {
    console.error("Error fetching user threads:", error);
    throw error;
  }
}

// 搜索用户 Almost similar to Thead (search + pagination) and Community (search + pagination)
export async function fetchUsers({
  userId,
  searchString = "",
  pageNumber = 1,
  pageSize = 20,
  sortBy = "desc",
}: {
  userId: string;
  searchString?: string;
  pageNumber?: number;
  pageSize?: number;
  sortBy?: SortOrder;
}) {
  try {
    connectToDB();

    // 根据页码和页面大小计算要跳过的用户数。
    const skipAmount = (pageNumber - 1) * pageSize;

    // 为提供的搜索字符串创建不区分大小写的正则表达式。
    const regex = new RegExp(searchString, "i");

    // 创建一个初始查询对象来过滤用户。
    const query: FilterQuery<typeof User> = {
      id: { $ne: userId }, // 从结果中排除当前用户。
    };

    // 如果搜索字符串不为空，请添加 $or 运算符以匹配用户名或名称字段
    if (searchString.trim() !== "") {
      query.$or = [
        { username: { $regex: regex } },
        { name: { $regex: regex } },
      ];
    }

    // 据createdAt字段和提供的排序顺序为获取的用户定义排序选项。
    const sortOptions = { createdAt: sortBy };

    const usersQuery = User.find(query)
      .sort(sortOptions)
      .skip(skipAmount)
      .limit(pageSize);

    // 计算符合搜索条件的用户总数（不分页）
    const totalUsersCount = await User.countDocuments(query);

    const users = await usersQuery.exec();

    // 检查当前页面之外是否还有更多用户
    const isNext = totalUsersCount > skipAmount + users.length;

    return { users, isNext };
  } catch (error) {
    console.error("Error fetching users:", error);
    throw error;
  }
}

/** 获取用户帖子新消息 */
export async function getActivity(userId: string) {
  try {
    connectToDB();

    // 查找该用户创建的所有线程
    const userThreads = await Thread.find({ author: userId });

    // 从每个用户线程的“children”字段收集所有子帖子 ID（回复）
    const childThreadIds = userThreads.reduce((acc, userThread) => {
      return acc.concat(userThread.children);
    }, []);

    // 查找并返回子帖子（回复），不包括同一用户创建的子帖子（回复）
    const replies = await Thread.find({
      _id: { $in: childThreadIds },
      author: { $ne: userId }, // 排除同一用户创建的帖子
    }).populate({
      path: "author",
      model: User,
      select: "name image _id",
    });

    return replies;
  } catch (error) {
    console.error("Error fetching replies: ", error);
    throw error;
  }
}
