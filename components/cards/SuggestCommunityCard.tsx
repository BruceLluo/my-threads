import Image from "next/image";
import Link from "next/link";

import { Button } from "../ui/button";

interface Props {
  id: string;
  name: string;
  username: string;
  imgUrl: string;
  bio: string;
  members: {
    image: string;
  }[];
}

function SuggestCommunityCard({
  id,
  name,
  username,
  imgUrl,
  bio,
  members,
}: Props) {
  return (
    <article className="community-card">
      <div className="flex flex-wrap items-center gap-3 justify-between">
        <div className="flex gap-3">
          {/* 社区头像 */}
          <Link href={`/communities/${id}`} className="relative h-12 w-12">
            <Image
              src={imgUrl}
              alt="community_logo"
              fill
              className="rounded-full object-cover"
            />
          </Link>
          {/* 社区名 */}
          <div>
            <Link href={`/communities/${id}`}>
              <h4 className="text-base-semibold text-black dark:text-light-1 text-ellipsis">{name}</h4>
            </Link>
            <p className="text-small-medium text-gray-1 text-ellipsis">@{username}</p>
          </div>
        </div>
        {/* 查看社区 */}
        <div className="gap-3">
          <Link href={`/communities/${id}`}>
            <Button size="sm" className="community-card_btn">
              View
            </Button>
          </Link>
        </div>
        {/* 社区介绍 */}
        {/* <p className="mt-4 text-subtle-medium text-gray-1">{bio}</p> */}
      </div>
    </article>
  );
}

export default SuggestCommunityCard;
