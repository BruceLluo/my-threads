"use client";
import { OrganizationSwitcher, SignedIn, SignOutButton } from "@clerk/nextjs";
import { dark } from "@clerk/themes";
import Image from "next/image";
import Link from "next/link";
import { ThemeSwitcher } from "../others/ThemeSwitcher";
import { useTheme } from "next-themes";

function Topbar() {
  const { theme, setTheme } = useTheme();

  return (
    <nav className="topbar">
      <Link href="/" className="flex items-center gap-4">
        <Image
          src="/assets/bruceluo-logo.svg"
          alt="logo"
          width={28}
          height={28}
        />
        <p className="text-heading3-bold dark:text-light-1 text-dark-1 max-xs:hidden">
          BruceLuo Threads
        </p>
      </Link>

      <div className="flex items-center gap-1">
        {/* clerk退出登录组件 移动端*/}
        <div className="block md:hidden">
          <SignedIn>
            <SignOutButton>
              <div className="flex cursor-pointer">
                <Image
                  src={theme === 'dark' ? "/assets/logout.svg" : '/assets/logout-dark.svg'}
                  alt="logout"
                  width={24}
                  height={24}
                />
              </div>
            </SignOutButton>
          </SignedIn>
        </div>
        <div className="hidden md:block">
          <ThemeSwitcher />
        </div>
        {/* clerk组织组件 */}
        <OrganizationSwitcher
          appearance={{
            baseTheme: theme === "dark" ? dark : undefined,
            elements: {
              organizationSwitcherTrigger: "py-2 px-4",
            },
          }}
        />
      </div>
    </nav>
  );
}

export default Topbar;
