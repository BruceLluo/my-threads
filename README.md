This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.


## create project step
1. 提交双仓库
```
git remote -v 查看当前仓库源
git remote set-url --add origin https://gitee.com/BruceLluo/my-threads.git
```
2. 创建项目
```
npx create-next-app@latest ./

npm install @clerk/nextjs @uploadthing/react mongoose svix uploadthing tailwindcss-animate @clerk/theme

```
3. clerk登录注册校验模块
```
使用next router 路由组，这允许您将路线段和项目文件组织到逻辑组中，而不影响 URL 路径结构。还可以嵌套路由，多个layout
(auth)->sign-in->sign-out
```
4. topbar顶部导航栏
```
[https://app.logo.com/dashboard/logo_4a0f393c-f0be-42c7-93f0-169ba515c728/your-logo-files]设置自己logo
[https://favicon.io/favicon-converter/] 生成icon
引入clerk组件
   {/* clerk退出登录组件 */}
        <div className='block md:hidden'>
          <SignedIn>
            <SignOutButton>
              <div className='flex cursor-pointer'>
                <Image
                  src='/assets/logout.svg'
                  alt='logout'
                  width={24}
                  height={24}
                />
              </div>
            </SignOutButton>
          </SignedIn>
        </div>

        {/* clerk组织组件 */}
        <OrganizationSwitcher
          appearance={{
            baseTheme: dark,
            elements: {
              organizationSwitcherTrigger: "py-2 px-4",
            },
          }}
        />
```
5. 左边导航栏
```
import { usePathname, useRouter } from "next/navigation";

useRouter，usePathname获取路由信息
```
6. 移动端底部导航栏
7. 完善登录信息流程onboarding
```
AccountProfile.tsx
引入shadcn ui 创建表单
npx shadcn-ui@latest init
npx shadcn-ui@latest add form input textarea

引入react-hook-form进行表单校验
引入zod 进行ts声明数据类型校验
引入uploadthing进行头像等资源上传，免费2g

使用nextjs中的server action 实验版
函数在服务器运行，但是可以在客户端调用
user.actions.ts中连接mongoose数据库，mongoosedb network access 设置允许所有ip 0.0.0.0./0
使用next的revalidatePath重新验证数据，并更新页面内容

提交表单上传头像，数据插入数据库
onboarding 同步数据库数据
```
8. 创建帖子,配置表单校验, 数据库model创建, thread action创建
9. 首页展示帖子, 创建ThreadCard
10. 帖子进行评论，展示回复贴子

11. 个人资料页,资料tabs,帖子

```
npx shadcn-ui@latest add tabs 
ThreadsTab
还有replies,tagged
```
12. 搜索用户页
13. activity 被回复的帖子页
14. 使用webhook svix监听clerk的Organization 各种事件，数据共享给数据库
```
api/webhook/clerk/route
```
15. 完善社区部分功能