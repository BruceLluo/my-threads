# 0.1.0 (2023-09-08)


### Bug Fixes

* **leftSidebar:** 修复刷新后image图标src与服务端不一致的问题 ([f2953e7](https://gitee.com/BruceLluo/my-threads/commits/f2953e7a87f7bb91d2dd1f074dd20300b15b9422))
* **login:** 修改样式,提供测试账号 ([e00f354](https://gitee.com/BruceLluo/my-threads/commits/e00f354d7bfe9500939ccd90ff810af494a7e422))
* **onboarding:** onboarding同步数据库数据,修改其他 ([41c1889](https://gitee.com/BruceLluo/my-threads/commits/41c1889c1b7b7415fdff1f4412f6b357f7680be4))
* **rightSideBar:** 社区设置为随机数据 ([528e09c](https://gitee.com/BruceLluo/my-threads/commits/528e09c33caa6eb4f6b90da002043805e7bf8912))
* **thread:** 帖子用户评论头像修改为数据库数据而不是clerk里的数据 ([e2c45b7](https://gitee.com/BruceLluo/my-threads/commits/e2c45b74cc836b36c8a7b017fda4d7073ac8be7c))


### Features

* **activity:** 用户帖子回复提示 ([3c47924](https://gitee.com/BruceLluo/my-threads/commits/3c47924cd126d4530bf22952e11961e5ee4adc93))
* **auth:** 项目初始化,引入clerk登录注册校验模块 ([b246664](https://gitee.com/BruceLluo/my-threads/commits/b246664d5f19f2e7f14610a61b56885bacb24e2f))
* **bottombar:** 移动端底部导航栏,部分右侧栏 ([a9e97bd](https://gitee.com/BruceLluo/my-threads/commits/a9e97bd460e8925808c4c302165ab3dcb63afe80))
* **comment:** 帖子回复使用表单isSubmitting状态,去除三个字以上才能发帖的限制 ([656c9e3](https://gitee.com/BruceLluo/my-threads/commits/656c9e3959fd784ece9994c5f8ce502f76b7cd53))
* **comment:** 帖子评论,展示 ([10610e9](https://gitee.com/BruceLluo/my-threads/commits/10610e908120b30c8d06c515635239163281b331))
* **community:** 补充社区页部分 ([4b974c0](https://gitee.com/BruceLluo/my-threads/commits/4b974c02ea721a50878c20d1e23337c2ac8a6893))
* **community:** 补充社区页部分 ([25592c8](https://gitee.com/BruceLluo/my-threads/commits/25592c85a5448292ce0496d2809f9ea5fee49260))
* **community:** 推荐社区模块基本完成 ([f4eb1a4](https://gitee.com/BruceLluo/my-threads/commits/f4eb1a4e03df8ee9f45ca6f7b6873b35b486becf))
* **create-thread:** 创建帖子 ([da0462f](https://gitee.com/BruceLluo/my-threads/commits/da0462f23fc34c88b759751d91ce86a7e3719efa))
* **create-thread:** 创建帖子 ([6d98a46](https://gitee.com/BruceLluo/my-threads/commits/6d98a46da656b5ead85d6d4bf6d7c9a6beeb96da))
* **home:** 获取帖子 ([4154b17](https://gitee.com/BruceLluo/my-threads/commits/4154b17080777c2704dc1fba77b08f5dacf79664))
* **home:** 首页帖子展现 ([372ed3e](https://gitee.com/BruceLluo/my-threads/commits/372ed3e6d6afa993d1a59564d551a317d67cb002))
* **leftbar:** 部分左边侧边栏 ([b5355ab](https://gitee.com/BruceLluo/my-threads/commits/b5355ab6a83f5bd35bd9a64e000adfeca59a274f))
* **loading:** 加入loading页 ([d32e40d](https://gitee.com/BruceLluo/my-threads/commits/d32e40d939f56e4da02b42029276dadc221cef2a))
* **onborading:** 登录后信息补充流程暂存 ([6e7e866](https://gitee.com/BruceLluo/my-threads/commits/6e7e8666a552b680d2d526d39848ef707742ee4d))
* **onborading:** 用户信息头像上传与存储数据库,clerk用户数据还没同步 ([ed922b1](https://gitee.com/BruceLluo/my-threads/commits/ed922b15dda9a897be27d81ad4ef9f7b10cdb5c6))
* **postThread:** 发布帖子,回复帖子loading ([4e0c788](https://gitee.com/BruceLluo/my-threads/commits/4e0c78841a7f1d46868f1d770c34dbe8dc3a29c9))
* **profile:** 个人资料页,完成用户个人帖子展示 ([4be4a8e](https://gitee.com/BruceLluo/my-threads/commits/4be4a8e9617274db037fe54a29df83a2a1de0f52))
* **search:** 用户搜索页 ([c7345d1](https://gitee.com/BruceLluo/my-threads/commits/c7345d1f915399aafae892d5c9ce5a102dfee604))
* **similar-minds:** 首页右侧栏推荐帖子 ([992c445](https://gitee.com/BruceLluo/my-threads/commits/992c4451d1d274014f17a5722df41f35de335faf))
* **theme:** 添加light主题颜色 ([0615732](https://gitee.com/BruceLluo/my-threads/commits/06157329116bc9772be3191033ce21d5cb886289))
* **theme:** 移动端主题配色补充 ([d7e7e60](https://gitee.com/BruceLluo/my-threads/commits/d7e7e60240e2e7a1a9ae1fc58e3f7bb2ea4bd574))
* **topbar:** 顶部导航栏,组织切换,退出登录 ([b4e1d59](https://gitee.com/BruceLluo/my-threads/commits/b4e1d5924408146051d3be549509d0524d80ecb3))
* **webhook:** 使用webhook监听clerk组织相关事件,存入数据库 ([1f0a4cb](https://gitee.com/BruceLluo/my-threads/commits/1f0a4cb5b4c6c303ad880eb7a1b22e2accf8a054))



