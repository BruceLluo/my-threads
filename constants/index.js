export const sidebarLinks = [
  {
    imgURL: "/assets/home.svg",
    imgURL_dark: "/assets/home-dark.svg",
    route: "/",
    label: "Home",
  },
  {
    imgURL: "/assets/search.svg",
    imgURL_dark: "/assets/search-dark.svg",
    route: "/search",
    label: "Search",
  },
  {
    imgURL: "/assets/heart.svg",
    imgURL_dark: "/assets/heart-dark.svg",
    route: "/activity",
    label: "Activity",
  },
  {
    imgURL: "/assets/create.svg",
    imgURL_dark: "/assets/create-dark.svg",
    route: "/create-thread",
    label: "Create Thread",
  },
  {
    imgURL: "/assets/community.svg",
    imgURL_dark: "/assets/community-dark.svg",
    route: "/communities",
    label: "Communities",
  },
  {
    imgURL: "/assets/user.svg",
    imgURL_dark: "/assets/user-dark.svg",
    route: "/profile",
    label: "Profile",
  },
];

export const profileTabs = [
  { value: "threads", label: "Threads", icon: "/assets/reply.svg" },
  { value: "replies", label: "Replies", icon: "/assets/members.svg" },
  { value: "tagged", label: "Tagged", icon: "/assets/tag.svg" },
];

export const communityTabs = [
  { value: "threads", label: "Threads", icon: "/assets/reply.svg" },
  { value: "members", label: "Members", icon: "/assets/members.svg" },
  { value: "requests", label: "Requests", icon: "/assets/request.svg" },
];

// 过渡时间
export const switchThemeDuration = "duration-200";