// "use client";
import React from "react";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { ClerkProvider } from "@clerk/nextjs";
import { dark } from "@clerk/themes";

import "../globals.css";
import { useTheme } from "next-themes";

const inter = Inter({ subsets: ["latin"] });

// export const metadata: Metadata = {
//   title: "BruceLuo Threads",
//   description: "Welcome to my threads application",
// };

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  // const { theme } = useTheme();
  return (
    <ClerkProvider
      // appearance={{
      //   baseTheme: theme === "dark" ? dark : undefined,
      // }}
    >
      <html lang="en">
        <body className={`${inter.className} dg-white dark:bg-dark-1`}>
          <div className=" flex justify-center items-center w-full min-h-screen">
            {children}
          </div>
        </body>
      </html>
    </ClerkProvider>
  );
}
