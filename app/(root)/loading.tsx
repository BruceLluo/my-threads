// Loading.tsx
export default function Loading() {
  return (
    <>
      <div className="dark:text-white flex justify-center items-center h-full w-full">Loading...</div>
    </>
  );
}