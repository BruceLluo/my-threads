import "../globals.css";
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import { ClerkProvider } from "@clerk/nextjs";
import Topbar from "@/components/shared/Topbar";
import Bottombar from "@/components/shared/Bottombar";
import LeftSidebar from "@/components/shared/LeftSidebar";
import RightSidebar from "@/components/shared/RightSidebar";
import { ThemeProvider } from "./theme-provider";
import { ThemeSwitcher } from "@/components/others/ThemeSwitcher";

const inter = Inter({ subsets: ["latin"] });

// 方便seo
export const metadata: Metadata = {
  title: "BruceLuo Threads",
  description: "Welcome to my threads application",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    // lang ="en"
    <ClerkProvider>
      <html lang="en">
        <body className={inter.className}>
          <ThemeProvider attribute="class" defaultTheme="white" enableSystem>
            {/* <ThemeSwitcher /> */}
            <Topbar />

            <main className="flex flex-row">
              <LeftSidebar />
              <section className="main-container">
                <div className="w-full h-full max-w-4xl">{children}</div>
              </section>
              <RightSidebar />
            </main>

            <Bottombar />
          </ThemeProvider>
        </body>
      </html>
    </ClerkProvider>
  );
}
